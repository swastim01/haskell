convert :: [Int] -> [Bool]
-- convert = map (\x -> if (rem x 2) == 0 then 'E' else 'O')
convert = map(\x -> even x)

distribute :: [Int] -> Int
distribute queue = if odd (sum queue) then -1 else distribute' queue

distribute' :: [Int] -> Int
distribute' = distribute''. convert

distribute'' :: [Bool] -> Int
-- distribute'' q@('E': _) = distribute'' $ tail q
distribute'' [] = 0
distribute'' q = if $ head q
                 then distribute'' (tail q)
                 else 2 + $ distribute'' $ [not (head q)] ++ (tail q)

main = do
  print $ distribute [1, 2, 1]
  print $ distribute [1, 1, 1]
  print $ distribute [1, 2, 2, 1]