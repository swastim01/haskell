next :: Int -> Int
next n = if (rem n 2) == 0 then (div n 2) else (3 * n + 1)

collatz :: Int -> [Int]
collatz 4 = [4, 2, 1]
collatz n = n : (collatz (next n))
-- collatz n = n ++ (collatz (next n))
-- you can always replace () with $

main = print $ collatz 7

{-

Python implementation of the same code:

def collatz(n: int) -> list[int]:
    if n == 4:
        return [4, 2, 1]
    else:
        return [n] + collatz(next_collatz(n))
-}