-- First haskell program
import Data.Char

digits :: Int -> [Int]
digits = map digitToInt . show

sumOfDigits :: Int -> Int
sumOfDigits = sum . digits


main = do 
  print $ digits 1729
  print $ sumOfDigits 1729

  print $ digits 128974563