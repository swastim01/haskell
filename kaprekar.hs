import Data.Char

numToDigits :: Int -> [Int]
numToDigits = map map digitToInt . show

digitsToNum :: [Int] -> Int
digitsToNum = foldl1 (\x y -> 10 * x + y)

largest :: Int -> Int
largest = digitsToNum . reverse . sort . numToDigits

smallest :: Int -> Int
smallest = digitsToNum . sort . numToDigits

next :: Int -> Int
next n = largest n - smallest n

kaprekar :: Int -> [Int]
kaprekar = iterate next

takeUntil :: Eq a => [a] -> [a]
takeUntil (x:x':xs) = if x == x' then [x] else x:takeUntil (x':xs)

main = do
  print $ numToDigits 1729
  print $ digitsToNum [1, 3, 5, 6, 6, 3]
  print $ kaprekar 1729