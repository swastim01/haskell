-- FizzBuzz

fbs :: [String]
fbs = zipWith (++) (cycle ["", "", "FIZZ"]) (cycle ["", "", "", "", "BUZZ"])

pick :: Foldable t => t a -> t a -> t a
pick a b = if null a then b else a

fizzbuzz :: [String]
fizzbuzz = zipWith pick fbs (map show [1..])

main = do
  print $ take 20 fizzbuzz